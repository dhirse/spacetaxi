function get_url_param(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);

    if (results == null)
        return "anzeigen";
    else
        return results[1];
}

function setter() {
    var scores = [];
    var score = get_url_param('score'); // Holen den Wert, der in der URL steht und speichern ihn in "score"
    var highscore = document.getElementById("AktuellerHighscore");
    var highscoreText = ""

    for (var i = 0; i < 10; i++) {
        scores[i] = read(i); // read --> verwenden des Browser storages
    }
    if (score != 'anzeigen') {
        highscoreText = "Herzlichen Glückwunsch ihre Punkte Betragen: " + score;
        scores.push(score);
    }
    highscoreText += "</br></br>BESTENLISTE: </br>";
    scores.sort(function(a, b) {
        return b - a;
    });
    for (var i = 0; i < 10; i++) {
        highscoreText += (i + 1) + ": " + scores[i] + "</br>";
        store(i, scores[i]);
    }
    highscore.innerHTML = highscoreText;
}

function store(key, value) {
    sessionStorage.setItem(key, value);
}

function read(key) {
    var score = sessionStorage.getItem(key);
    if (score == 'undefined') {
        score = 0;
    }
    return Number(score);
}

document.addEventListener("DOMContentLoaded", setter, false);