// Objekt Taxi
function taxi() {
    this.getX = 0;
    this.getY = 0;
    this.schubY = 0;
    this.schubX = 0;
    this.getSchub = false;
    this.rotateLeft = false;
    this.rotateRight = false;
    this.kolision = false;
    this.firstKolision = true; // Sicherheitsmechanismus für JS um einen Overflow an Kollisionsabfragen zu vermeiden
    this.landing = false;
    this.getRotation = 0;
    this.counter = 0;
    this.reset = function() {
        this.schubX = 0;
        this.schubY = 0;
        getRotation = 0;
        getSchub = false;
    };
    this.updateLeben = function() {
        if (this.kolision && this.firstKolision) { //alle 20ms crash wenn nicht vorhanden aufgrund der update funktion
            g_leben--;
            g_peng.play();
            this.firstKolision = false;
        }
    }
    this.setRotation = function() {
        if (!this.landing) {
            if (this.rotateLeft) {
                if (this.getRotation <= 0) { // um einen 360 Grad Wert abbilden zu können
                    this.getRotation = 360;
                }
                this.getRotation -= 5;
            }
            if (this.rotateRight) {
                if (this.getRotation >= 360) {
                    this.getRotation = 0;
                }
                this.getRotation += 5;
            }
        }
    };

    this.update = function() {
        if (!this.kolision) {
            this.Kolision_Rahmen();
            this.Kolision_Platform();
            this.Kolision_passagier();
            this.Kolision_ausgang();
            this.updateLeben();
            if (this.landing) {
                if (this.getRotation <= 10) { // stellt das Raumschiff gerade auf eine Plattform, nach dem Landen
                    while (this.getRotation != 0) {
                        this.getRotation--;
                    }
                }
                if (this.getRotation >= 350) {
                    while (this.getRotation != 360) {
                        this.getRotation++;
                    }
                }
            }
            if (!this.kolision) {
                if (this.getSchub) {

                    this.schubX += 0.025 * Math.sin(this.getRotation * Math.PI / 180); // Math.Pi.....  Umrechnung von Radiant auf Grad
                    this.schubY += 0.025 * Math.cos(this.getRotation * Math.PI / 180);
                    g_raumschiff.play();

                } else {
                    g_raumschiff.pause(); //Kein Schub, kein Sound
                    if (this.schubY > -0.25 && !this.landing) {
                        this.schubY -= 0.02;
                    }
                    if (this.landing) {
                        this.schubY = 0;
                        this.schubX = 0;
                    }
                    if (this.schubX > 0) {
                        this.schubX -= 0.0005;
                    } else if (this.schubX < 0) {
                        this.schubX += 0.0005;
                    }
                }
                this.getY -= this.schubY;
                this.getX += this.schubX;
            }
        }
    };
    this.Kolision_Rahmen = function() {
        for (i = 0; i < g_rahmen.length; i++) {
            g_rahmen[i].hasKolision(this);
            if (g_rahmen[i].kolision) {
                this.kolision = true;
                //console.log("kolision=" + this.kolision);
            }
        }
    };
    this.Kolision_Platform = function() {
        for (i = 0; i < g_plattform.length; i++) {
            g_plattform[i].hasKolision(this);
            if (g_plattform[i].landing) {
                this.landing = true;
                this.kolision = false;
                return null;
            } else if (g_plattform[i].kolision) {
                this.kolision = true;
                this.landing = false;
                //console.log("tets kolision=" + this.kolision);
                return null;
            } else {
                this.landing = false;
            }
        }
    }
    this.Kolision_passagier = function() {
        if (this.landing) {
            for (i = 0; i < g_passagier.length; i++) {
                g_passagier[i].Kolision_passagier(this);
                if (g_passagier[i].zusteigen) {
                    this.counter++;
                    console.log("Passagier aufgenommen!");
                }
            }
        }
    }
    this.Kolision_ausgang = function() {
        for (i = 0; i < g_ausgang.length; i++) {
            g_ausgang[i].hasKolision(this);
            if (g_ausgang[i].kolision) {
                if (this.counter < g_passagier.length) {
                    this.kolision = true;
                } else {
                    g_level++;
                    var timer = getTimer();
                    g_timer.push(timer.getTime()); // Speichern die aktuell gebrauchte Zeit für das jeweilige Level
                    LadeLevel(g_level);
                }
            }
        }
    };
    this.addLive = function() {
        g_leben++;
    }
}