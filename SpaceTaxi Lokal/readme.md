# Space Taxi
Ein Remake des bekannten Spiels Space Taxi. Spielbar ist es innerhalb des Browsers und erreichbar unter:
- Wordpress: http://hirse.hol.es/spacetaxi/ (Hier gibt es aufgrund der Themebreite Darstellungsprobleme)
- Online: http://hirse.hol.es/wp-content/uploads/2016/08/Hauptmenue.html

## Spielprinzip

- Sammle die Monster nach einander ein
- Fliege dabei nicht gegen die Wände
- Verlasse das Level durch die sich öffnende Schleuse
- Findet widererwartens eine Kollision mit einer Ebene oder des Randes statt, verliert man ein Leben
- Es gibt drei Leben
- Um eine möglichst hohe Punktzahl zu erreichen, ist **Geschwindigkeit** von nöten!

## Steuerung
- W / Pfeil hoch: Schub    
- D / Pfiel rechts : Rechte Rotation
- A / Pfeil links : Linke Rotation 
- P:Pause
- F:Cheat 

## Grafiken
- Diverse Hintergrundbilder für Das Spiel, die Menüs oder den Highscore sind selbstgezeichnet
- Passagiere, Taxi, Plattform, Wände und die Lebensanzeige sind selbstgezeichnet

## Sounds
- Sounds für Explosion, öffnen der Schleuse, Schub, Passagiere sind selbst aufgenommen
- Das Hintergrundtheme steht unter der CC

## Lizensen
- Background Theme https://www.freesound.org/people/Setuniman/sounds/328779/ Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
- Apache APACHE LICENSE, VERSION 2.0 (CURRENT)

## Authoren
- Mario Trültzsch
- David Hirsekorn

## Aufgabenteilung
- Code: Mario Trültzsch
- Sound + Grafik + Code(less): David Hirsekorn

## Levelaufbau
- R: Rahmen
- ".": Luft
- E: Ausgang
- "#": Plattform
- 1,2,3: Passagiere
- T: Taxi

##### Made with ♥ by Mario Trültzsch & David Hirsekorn