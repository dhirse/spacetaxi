document.addEventListener("DOMContentLoaded", init, false);

function init() {
    document.getElementById("starteSpiel").addEventListener("click", function() {
        var selection = document.getElementById("groupSelection").value;
        window.location.href = "http://localhost/SpaceTaxi Lokal/spielfeld.html?group=" + selection;
    });
    document.getElementById("highscore").addEventListener("click", function() {
        window.location.href = "http://localhost/SpaceTaxi Lokal/highscore.html"
    });

    getSelectOptions();
}

function getSelectOptions() {
    var selection = document.getElementById("groupSelection");
    for (var i = 1; i < 7; i++) {
        var url = "level" + i + addZero(1) + ".map";
        if (UrlExists(url)) {
            var option = document.createElement("option");
            option.text = i;
            option.value = i;
            selection.add(option);
        }
    }
}

function UrlExists(url) {
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}