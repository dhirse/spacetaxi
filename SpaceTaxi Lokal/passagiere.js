// Objekt Passagiere
function passagiere(x, y) {
    this.getX = x * 25;
    this.getY = y * 25;
    this.showpassagier = true;
    this.zusteigen = false;
    this.Kolision_passagier = function(taxi) {
        if (this.showpassagier) {
            var mittelpunktX = this.getX + (25 / 2);
            var mittelpunktY = Math.floor(this.getY + (25 / 2));
            var mittelpunkTaxiX = taxi.getX + (25 / 2);
            var mittelpunktTaxiY = Math.floor(taxi.getY + (25 / 2));
            if ((mittelpunktX >= mittelpunkTaxiX - 25 && mittelpunktX <= mittelpunkTaxiX) || (mittelpunktX <= mittelpunkTaxiX + 25 && mittelpunktX >= mittelpunkTaxiX) && (mittelpunktY == mittelpunktTaxiY)) {
                this.showpassagier = false;
                this.zusteigen = true;
                g_monster.play();
            }
        } else {
            this.zusteigen = false;
        }
    }
}