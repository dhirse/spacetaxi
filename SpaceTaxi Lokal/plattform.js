// Objekt Plattform
function plattform(x, y) {
    this.getX = x * 25;
    this.getY = y * 25;
    this.kolision = false;
    this.landing = false;
    this.hasKolision = function(taxi) {//obere linke Ecke
        if ((this.getX <= taxi.getX && taxi.getX <= this.getX + 25) &&
            (this.getY <= taxi.getY && taxi.getY <= this.getY + 25)) {
            this.kolision = true;
            this.landing = false;
        } //untere linke Ecke
        else if ((this.getX <= taxi.getX && taxi.getX <= this.getX + 25) &&
            (this.getY <= taxi.getY + 25 && taxi.getY + 25 <= this.getY + 25)) {
            if (taxi.schubY >= 0.3 || taxi.schubY <= -0.3) {
                this.kolision = true;
                this.landing = false;
            } 
            else {
                if ((taxi.getRotation >= 350 || taxi.getRotation <= 10)) {
                    this.kolision = false;
                    this.landing = true;
                } 
                else {
                    this.kolision = true;
                    this.landing = false;
                }
            }
        } //obere rechte Ecke
        else if ((this.getX <= taxi.getX + 25 && taxi.getX + 25 <= this.getX + 25) &&
            (this.getY <= taxi.getY && taxi.getY <= this.getY + 25)) {
            this.kolision = true;
            this.landing = false;
        } // untere rechte Ecke
        else if ((this.getX <= taxi.getX + 25 && taxi.getX + 25 <= this.getX + 25) &&
            (this.getY <= taxi.getY + 25 && taxi.getY + 25 <= this.getY + 25)) {
            if (taxi.schubY >= 0.3 || taxi.schubY <= -0.3) {
                this.kolision = true;
                this.landing = false;
            } 
            else {
                if ((taxi.getRotation >= 350 || taxi.getRotation <= 10)) {
                    this.kolision = false;
                    this.landing = true;
                } 
                else {
                    this.kolision = true;
                    this.landing = false;
                }
            }
        } 
        else {
            this.kolision = false;
            this.landing = false;
        }
    }
}