function LadeLevel(level, reset) {
    g_rahmen = [];
    g_ausgang = [];
    g_plattform = [];
    g_hindernis = [];
    g_taxi = new taxi();
    g_passagier = [];
    g_startzeit = Date.now();
    clearTimeout(timeout);

    var httpRequest;
    var levelBeschreibung;
    httpRequest = new XMLHttpRequest(); // Startes eine Anfrage an den WebServer

    httpRequest.addEventListener('readystatechange', function(event) {
        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
            levelBeschreibung = httpRequest.responseText; // Inhalt der geladenen .map Dateien
            Analysetext(levelBeschreibung, reset);
            update();
        }
        if (httpRequest.status == 404) {
            g_ende = true;
            redirektToHighscore();
        }
    });

    var url = "level" + g_group + addZero(level) + ".map";
    httpRequest.open("GET", url, true);
    httpRequest.send();

}

function Analysetext(text, reset) {
    var textRows = text.split("\n"); //Neue Zeile wenn \n
    var hasLiveSet = false;
    for (i = 0; i < textRows.length; i++) { // Iteration über alle Zeilen
        var textRow = textRows[i];
        for (j = 0; j < textRow.length; j++) { // Interpretation der Zeichen einer Zeile
            switch (textRow[j]) {
                case "R":
                    g_rahmen.push(new rahmen(j, i));
                    break;
                case "#":
                    g_plattform.push(new plattform(j, i));
                    break;
                case "T":
                    g_taxi.getX = j * 25;
                    g_taxi.getY = i * 25;
                    break;
                case "1":
                    g_passagier.push(new passagiere(j, i));
                    break;
                case "2":
                    g_passagier.push(new passagiere(j, i));
                    break;
                case "3":
                    g_passagier.push(new passagiere(j, i));
                    break;
                case "E":
                    g_ausgang.push(new ausgang(j, i));
                    break;
                case "L":
                    if (!reset) {
                        g_taxi.addLive();
                        3
                        this.hasLiveSet = true;
                    }
                    break;
            }
        }
    }
    if (!this.hasLiveSet) {
        if (!reset) { // Wenn Enter gedrückt wird, wird das Leben NICHT neu gesetzt
            g_leben = 3;
        }
    }
}