document.addEventListener("DOMContentLoaded", init, false); // Wenn alles geladen ist, führe die init-Funcion aus

var g_rahmen = []
var g_ausgang = [];
var g_plattform = [];
var g_hindernis = [];
var g_timer = [];
var g_taxi = new taxi();
var g_passagier = [];
var g_ctx;
var g_canvas;
var g_level;
var g_ende = false;
var g_startzeit; // g_startzeit = aktueller Zeitstempel.
var timeout;
var test_counter = 0;
var g_leben = 0;
var g_paused = false;
var g_group = 5;
var showAusgang;

// Laden der einzelnen Imgaes, welche innerhalb des Canvas-Elements gezeichnet werden
var g_rahmenImg = document.createElement('img');
g_rahmenImg.src = 'assets/rahmen.jpg';
var g_ausgangImg = document.createElement('img');
g_ausgangImg.src = 'assets/ausgang.jpg';
var g_plattformImg = document.createElement('img');
g_plattformImg.src = 'assets/plattform.jpg';
var g_hindernisImg = document.createElement('img');
g_hindernisImg.src = 'assets/hindernis.png';
var g_taxiImg = document.createElement('img');
g_taxiImg.src = 'assets/taxi.png';
var g_passagierImg = document.createElement('img');
g_passagierImg.src = 'assets/passagier.png';
var g_taxiImg_boom = document.createElement('img');
g_taxiImg_boom.src = 'assets/feuer2.png';


// Initiierungsfunktion für das Canvas-Element und die Level. Begonnen wird mit Level 1
function init() {
    g_level = 1;
    g_canvas = document.getElementById("Spielfeld");
    g_ctx = g_canvas.getContext("2d");
    var group = get_url_param("group");
    if (group != null) {
        g_group = group;
    }
    LadeLevel(g_level, false);
}

function get_url_param(name) { //Um URL leserlicher zu übergeben
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);

    if (results == null)
        return null;
    else
        return results[1];
}

// In dieser Funktion werden die Keycodes abgefragt, wenn eine bestimmte Taste gedrückt wird
document.addEventListener("keydown", function(event) {
    /*console.log("KeyEvent: " + event.keyCode + " ->" + test_counter);
    test_counter++;*/

    // 80 = p
    if (event.keyCode == 80) {
        if (g_paused) {
            g_paused = false;
            update(); // wenn p NICHT gedrückt wird, wird die update-Funktion aufgerufen um das Spielfeld alle 20ms neu zu zeichnen
        } else {
            g_paused = true;
        }
    }
    // 38 = Pfeiltaste oben; 87 = w
    if (event.keyCode == 38 || event.keyCode == 87) {
        g_taxi.getSchub = true;
        event.preventDefault(); // bei Druck der o.g. Tasten, wird das Scrollen im Browser verhindert
    }
    // 40 = Pfeiltaste unten
    if (event.keyCode == 40) {
        event.preventDefault(); // bei Druck der o.g. Tasten, wird das Scrollen im Browser verhindert
    }
    // 37 = Pfeiltaste links; 65 = a
    if (event.keyCode == 37 || event.keyCode == 65) {
        g_taxi.rotateLeft = true;
        g_taxi.setRotation(); // Aufruf der Rotationsfunktion im Objekt Taxi (taxi.js)
    }
    // 39 = Pfeiltaste rechts; 68 = d
    if (event.keyCode == 39 || event.keyCode == 68) {
        g_taxi.rotateRight = true;
        g_taxi.setRotation(); // Aufruf der Rotationsfunktion im Objekt Taxi (taxi.js)
    }
    // 70 = f || Cheatfunktion zum Testen
    if (event.keyCode == 70) {
        for (i = 0; i < g_passagier.length; i++) {
            g_passagier[i].showpassagier = false;
            g_passagier[i].zusteigen = true;
            g_taxi.counter++;
        }
    }
    // 13 = Return
    if (event.keyCode == 13) {
        if (!g_taxi.kolision) {
            g_leben--;
        }
        if (g_leben > 0) {
            g_ende = false;
            LadeLevel(g_level, true);
        } else {
            redirektToHighscore();
        }

    }
});

// In dieser Funktion werden die Keycodes abgefragt, wenn eine bestimmte Taste NICHT gedrückt wird
document.addEventListener("keyup", function(event) {

    if (event.keyCode == 38 || event.keyCode == 87 || event.keyCode == 37 || event.keyCode == 65) {
        g_taxi.getSchub = false;
    }
    if (event.keyCode == 37 || event.keyCode == 65) {
        g_taxi.rotateLeft = false;
        g_taxi.setRotation();
    }
    if (event.keyCode == 39 || event.keyCode == 68) {
        g_taxi.rotateRight = false;
        g_taxi.setRotation();
    }
});

function redirektToHighscore() {
    var endpunkte = 0;
    for (var i = 0; i < g_timer.length; i++) {
        endpunkte += ((i + 1) * 100 / g_timer[i]) * 10000000; // Berechnung der Punkte für ein Level. Hier wird der Wert angefordert, (timer) welcher in "ZeichneSpielfeld" ermittelt wird
    }
    endpunkte = Math.floor(endpunkte); // Punktestand wird zu einer ganzen Zahl aufgerundet
    window.location = "http://localhost/SpaceTaxi Lokal/highscore.html?score=" + endpunkte;
}