// Objekt Rahmen
function rahmen(x, y) {
        this.getX = x * 25,
        this.getY = y * 25,
        this.kolision = false,
        this.hasKolision = function(taxi) {//obere linke Ecke
            if ((this.getX <= taxi.getX && taxi.getX <= this.getX + 25) &&
                (this.getY <= taxi.getY && taxi.getY <= this.getY + 25)) {
                this.kolision = true;
            } //untere linke Ecke
            else if ((this.getX <= taxi.getX && taxi.getX <= this.getX + 25) &&
                (this.getY <= taxi.getY + 25 && taxi.getY + 25 <= this.getY + 25)) {
                this.kolision = true;
            } //obere rechte Ecke
            else if ((this.getX <= taxi.getX + 25 && taxi.getX + 25 <= this.getX + 25) &&
                (this.getY <= taxi.getY && taxi.getY <= this.getY + 25)) {
                this.kolision = true;
            } //untere rechte Ecke
            else if ((this.getX <= taxi.getX + 25 && taxi.getX + 25 <= this.getX + 25) &&
                (this.getY <= taxi.getY + 25 && taxi.getY + 25 <= this.getY + 25)) {
                this.kolision = true;
            } 
            else {
                this.kolision = false;
            }
        }
}