// Es weren alle Funktionen aufgerufen, um das Spielfeld 1 mal zeichnen zu können
function ZeichneSpielfeld() {
    ZeichneHintergrund();
    ZeichneRahmen();
    ZeichnePlattform();
    ZeichnePassagier();
    ZeichneAusgang();
    ZeichneLebensanzeige();
    ZeichneTaxi();
}

function update() {
    if (!g_paused) {
        ZeichneSpielfeld();
        g_taxi.update();
        setTime();
        timeout = setTimeout(update, 20); // die update-Funktion ruft sich alle 20ms selber auf
    }
}

// Es wird die Spieldauer pro Level berechnet und angezeigt
function setTime() {
    var counterElement = document.getElementById("counter");
    var innerTimerText = "";
    for (var i = 0; i < g_timer.length; i++) {
        var timer = new Date(g_timer[i]);
        innerTimerText += "Level " + (i + 1) + ": " + formatTime(timer) + "<br/>";
    }
    var timer = getTimer(); // Der Inhalt dieser Variablen ist gleich der übergebene Wert der Funktion getTimer()
    innerTimerText += "Level " + g_level + ": " + formatTime(timer);
    counterElement.innerHTML = innerTimerText;
}

// Beim Return wird die Funktion addZero aufgerufen um eine voranstehende Null anzuzeigen
function formatTime(timer) {
    return addZero(timer.getMinutes()) + ":" + addZero(timer.getSeconds()) + ":" + timer.getMilliseconds();
}

// Die aktuelle Zeil wird ermittelt 
function getTimer() {
    var actuelTime = Date.now(); // gibt die aktuelle Zeit
    var timer = new Date(actuelTime - g_startzeit); // Erstellen eines neuen Datums-Objekt
    return timer;
}

// aus 1:50:12 M/SS/NN wird 01:50:12 MM/SS/NN
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function ZeichneLebensanzeige() {
    var lebensanzeige = document.getElementById("Lebensanzeige");
    var leben = ""; // Variable wird gefüllt mit dem unten angegebenen Image
    for (var i = 0; i < g_leben; i++) {
        leben += "<img src=\"assets/Lebensanzeige.jpg\"/>";
    }
    lebensanzeige.innerHTML = leben;
}

function ZeichneRahmen() { // Iteration über alle Rahmenelemente mit anschließendem Zeichnen
    for (i = 0; i < g_rahmen.length; i++) {
        ZeichneElement(g_rahmen[i], g_rahmenImg); // Aufruf der Funktion ZeichenElement (mit Übergabe 2er Parameter) um die Position und Größe des zu zeichnenden Elementes festzulegen
    }
}

function ZeichneElement(position, element, rotation) {
    if (rotation != undefined) { //Zeichne elemente wenn Rotation definiert (taxi, da einziges element das rotiert)
        g_ctx.save(); // Standartfunktion vom Canvas Element, speichert alle Element auf Canves
        g_ctx.translate(position.getX + (25 / 2), position.getY + (25 / 2)); // Bewegung des Ausgangspunktes auf das Zentrum des zu zeichneden Bildes
        g_ctx.rotate(g_taxi.getRotation * Math.PI / 180); // Rotationn des Canvas Elements
        g_ctx.drawImage(element, -(25 / 2), -(25 / 2), 25, 25); // Zeichnen des Bildes an der richtigen Stelle
        g_ctx.restore(); // Parameter auf Ausgangsposition zurücksetzen
    } else {
        g_ctx.drawImage(element, position.getX, position.getY, 25, 25);
    }
}

function ZeichnePlattform() { // Iteration über alle Plattformelemente mit anschließendem Zeichnen
    for (i = 0; i < g_plattform.length; i++) {
        ZeichneElement(g_plattform[i], g_plattformImg);
    }
}

function ZeichneTaxi() {
    if (g_taxi.kolision) {
        g_taxi.reset(); //Bei einer Kollision werden alle Werte wie z.B. Rotation und Schub des Taxis wieder auf Null gesetzt
        ZeichneElement(g_taxi, g_taxiImg_boom); //Einblenden einer Kollisionsgrafik an der Position des Taxis
    } else {
        ZeichneElement(g_taxi, g_taxiImg, true);
    }
}

function ZeichnePassagier() {
    for (i = 0; i < g_passagier.length; i++) {
        if (g_passagier[i].showpassagier) {
            ZeichneElement(g_passagier[i], g_passagierImg);
        }
    }
}

g_isFristTime = true;

function ZeichneAusgang() {
    if (g_taxi.counter < g_passagier.length) {
        for (i = 0; i < g_ausgang.length; i++) {
            ZeichneElement(g_ausgang[i], g_ausgangImg);
        }
        g_isFristTime = true;
    } else {
        if (g_isFristTime) {
            g_schleuse.play(); //Sound wird abgespielt
            g_isFristTime = false;
        }
    }
}

function ZeichneHintergrund() {
    if (!g_ende) {
        var hintergrund = document.createElement('img');
        hintergrund.src = 'assets/hintergrund.jpg';
        g_ctx.drawImage(hintergrund, 0, 0, 800, 600);
    }
}